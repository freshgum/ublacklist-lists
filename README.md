# freshgum's uBlacklist Collection

A collection of personally-maintained, up-to-date [uBlacklist](https://iorate.github.io/ublacklist/docs) 
lists for removing certain types of content from search results.

Rules are separated into categories (blogspam, SEO farming, development spam, etc.).

Currently, all rules are in the [`rules.txt`](./rules.txt) file (mainly because it's just easier to work with).
However, I'm planning on making a script that separates the sections in there into separate files. 
